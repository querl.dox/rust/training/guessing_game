#![doc(html_logo_url = "crown.svg")]
#![doc(html_favicon_url = "favicon.ico")]

//! # Programming a Guessing Game
//!
//! We’ll implement a classic beginner programming problem: a guessing game.
//! Here’s how it works: the program will generate a random integer between 1 and 100.
//!
//! It will then prompt the player to enter a guess.
//! After a guess is entered, the program will indicate whether the guess is too low or too high.
//!
//! If the guess is correct, the game will print a congratulatory message and exit.
//!

// This code contains a lot of information, so let’s go over it line by line.
// To obtain user input and then print the result as output, we need to bring the io (input/output) library into scope.
// The io library comes from the standard library (which is known as std):
use std::io;


/// Guess a number.
///
/// # Examples
/// ```
/// pub fn main () {
///    // Display prompt.
///    println!("Guess the number!");
///    // Display prompt.
///    println!("Please input your guess.");
///    // Create a place to store the user input.
///    let mut guess = String::new(); 
///    // Call the stdin function from the io module.
///    // .read_line(&mut guess), calls the read_line method on the standard input handle to get input from the user.
///    // Pass one argument to read_line: &mut guess.
///    io::stdin().read_line(&mut guess)
///        .expect("Failed to read line"); 
///    // Display the guessed number.
///    println!("You guessed: {}", guess);
/// }
/// ```

// The main function is the entry point into the program:
pub fn main () {
    println!("Guess the number!");
    println!("Please input your guess.");
    let mut guess = String::new();
    
    io::stdin().read_line(&mut guess)
        .expect("Failed to read line");
    println!("You guessed: {}", guess);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn guess_88() {
        let guess = 88;
        assert_eq!(main, 88);
    }
}
